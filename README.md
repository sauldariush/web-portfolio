# Web Portfolio README

This repository contains the code for my personal web portfolio, showcasing my projects, skills, and experiences. The portfolio was built using Vite, React, and Material-UI (MUI), and it's deployed on Vercel. 

## Table of Contents

- [Demo](#demo)
- [Features](#features)
- [Technologies](#technologies)
- [Prerequisites](#prerequisites)
- [Usage](#usage)
- [Deployment](#deployment)


## Demo

Check out the live portfolio: [Portfolio Demo](https://www.sauldariush.bio)

## Features

- **Project Showcase**: Displayed my projects with descriptions, screenshots, and links.
- **Skills**: Highlight my skills and expertise.
- **About Me**: A brief overview of about me.
- **Responsive Design**: The portfolio is designed to look great on various devices and screen sizes.
- **Contact**: To provide a way for the visitors to get in touch with me.

## Technologies

- **Vite**: Fast build tool and development server.
- **React**: JavaScript library for building user interfaces.
- **Material-UI (MUI)**: React UI framework that provides pre-designed components.
- **Vercel**: Cloud platform for serverless deployment.


## Prerequisites

- [Node.js](https://nodejs.org) (>= 14.x)
- [npm](https://www.npmjs.com/) package manager



## Usage
To start the development server, run the following command:

```bash
npm run dev
```

This will launch the portfolio in your browser at http://localhost:3000.

## Deployment
The portfolio is designed to be easily deployed on Vercel. These are the steps for deploying:

1. Create an account on Vercel if you don't have one.
2. Connect your GitHub account to Vercel.
Import your portfolio repository.
3. Configure the deployment settings (if necessary).
4. Deploy the project.

Vercel will automatically build and deploy whenever changes are pushed to the GitHub repository.
