import { Container } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

import './App.css'

import AppNavBar from './components/AppNavBar';
import About from '../pages/About';
import Contact from '../pages/Contact';
import Gallery from '../pages/Gallery';
import Home from '../pages/Home';
import Projects from '../pages/Projects';

const theme = createTheme({
  typography: {
    fontFamily: [
      'Cairo',
    ].join(','),
  },});

function App() {
  
  return (
    <ThemeProvider theme={theme}>
        <Container>
          <Router>
            <AppNavBar />
              <Routes> 
                <Route path='/' element={<Home />}/>
                <Route path='/about' element={<About />}/>
                <Route path='/contact' element={<Contact />}/>
                <Route path='/gallery' element={<Gallery />}/>
                <Route path='/projects' element={<Projects />}/>
              </Routes>
          </Router>
        </Container>
    </ThemeProvider>
  )
}
export default App
