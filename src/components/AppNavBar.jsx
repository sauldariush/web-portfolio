import * as React from 'react';
import PropTypes from 'prop-types';
import { 
  AppBar, 
  Box,
  CssBaseline,
  Divider,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  Toolbar,
  Typography,
  Button
 } from '@mui/material';
import { Link as RouterLink} from 'react-router-dom';

import MenuIcon from '@mui/icons-material/Menu';
import { Collections, Home, Person, ViewInAr } from '@mui/icons-material';

const ContactLinkBehavior = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/contact" {...props} role={undefined} />
));
const HomeLinkBehavior = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/" {...props} role={undefined} />
));
const AboutLinkBehavior = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/about" {...props} role={undefined} />
));
const ProjectsLinkBehavior = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/projects" {...props} role={undefined} />
));
const GalleryLinkBehavior = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/gallery" {...props} role={undefined} />
));

const drawerWidth = 240;

function AppNavbar(props) {

  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
      <Typography variant="h6" sx={{ my: 2, fontWeight: 900, fontSize: 30  }}>
        SDHSE
      </Typography>
      <Divider />
      <List>
        <ListItem disablePadding>
            <ListItemButton sx={{ textAlign: 'center' }} component={HomeLinkBehavior}>
              <ListItemText primary='Home' />
            </ListItemButton>
            
          </ListItem>
          <ListItem disablePadding>
            <ListItemButton sx={{ textAlign: 'center' }} component={AboutLinkBehavior}>
              <ListItemText primary='About' />
            </ListItemButton>
            
          </ListItem>
          <ListItem disablePadding>
            <ListItemButton sx={{ textAlign: 'center' }} component={GalleryLinkBehavior}>
              <ListItemText primary='Gallery'/>
            </ListItemButton>
            
          </ListItem>
          <ListItem disablePadding>
            <ListItemButton sx={{ textAlign: 'center' }} component={ProjectsLinkBehavior}>
              <ListItemText primary='Projects' />
            </ListItemButton>
            
          </ListItem>
          <ListItem disablePadding>
            <ListItemButton sx={{ textAlign: 'center' }} component={ContactLinkBehavior}>
              <ListItemText primary='Contact' />
            </ListItemButton>
            
          </ListItem>

      </List>
    </Box>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar component="nav" sx={{ backgroundColor: '#fff'}} >
        <Toolbar>

            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              sx={{ mr: 2, display: { sm: 'none' } }}
              
            >
              <MenuIcon sx={{ color: 'black'}} />
            </IconButton>


          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' }, color: '#212121', fontWeight: 900, fontSize: 30 }}
          >
            SDHSE
          </Typography>
          <Box sx={{ display: { xs: 'none', sm: 'flex' }, flexDirection: 'row', alignItems: 'center' }}>
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginX: 2 }}>
                <IconButton component={HomeLinkBehavior}>
                  <Home fontSize='large' sx={{ color: '#212121'}}/>
                </IconButton>
              <Typography variant='caption' align='center' sx={{ color: '#212121'}}>Home</Typography>
            </Box>
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginX: 2 }}>
              <IconButton component={AboutLinkBehavior}>
                <Person fontSize='large' sx={{ color: '#212121' }}/>
              </IconButton>
              <Typography variant='caption' align='center' sx={{ color: '#212121'}}>About</Typography>
            </Box>
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginX: 2  }}>
              <IconButton component={GalleryLinkBehavior}>
                <Collections fontSize='large' sx={{ color: '#212121' }}/>
              </IconButton>
              <Typography variant='caption' align='center' sx={{ color: '#212121'}}>Gallery</Typography>
            </Box>
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginX: 2 }}>

              <IconButton component={ProjectsLinkBehavior}>
                <ViewInAr fontSize='large' sx={{ color: '#212121' }}/>
              </IconButton>
              <Typography variant='caption' align='center' sx={{ color: '#212121'}}>Projects</Typography>
            </Box>
              <Button variant='contained' sx={{ color: '#fff', backgroundColor: 'gray', marginX: 2, paddingX: 5}} 
              component={ContactLinkBehavior}
              >
                Contact
              </Button>
          </Box>
        </Toolbar>
      </AppBar>
      <Box component="nav">
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          {drawer}
        </Drawer>
      </Box>
 
    </Box>
  );
}

AppNavbar.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default AppNavbar;
