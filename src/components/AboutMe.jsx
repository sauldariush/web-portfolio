import { Avatar, Box, Grid, Typography, IconButton, Button } from '@mui/material';
import { Email, GitHub, LinkedIn, Phone } from '@mui/icons-material';
import React from 'react'

function AboutMe() {
  return (
    <Box >
        <Typography variant='h3' sx={{ textAlign: 'center' }}>About Me</Typography>
        <Grid container rowSpacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }} sx={{ marginTop: 1 }}>
            <Grid 
                item 
                xs={12} 
                md={6}           
                sx={{
                    order: { xs: 2, md: 1 },
                    alignItems: {xs: 'center', md: 'end'},
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                }}
            >
                <Typography variant='h4' sx={{ fontSize: {xs:30, sm: 40}}}>Saul Darius Hernandez</Typography>
                <Typography variant='h6'sx={{ fontSize: {xs:17, sm: 25}}}>Electrical Engineer / Software Engineer</Typography>
                <Typography variant='subtitle2'>Email: sauldariush@gmail.com</Typography>
                <Typography variant='subtitle2'>Mobile Number: +63 949 883 0134</Typography>
                <Box sx={{ marginY: 1 }}>
                    <IconButton sx={{ marginX: 2 }} href="mailto:sauldariush@gmail.com" target='_blank'>
                        <Email sx={{ color: 'black' }} />
                    </IconButton>
                    <IconButton sx={{ marginX: 2 }} href='https://github.com/dariusgithub14' target='_blank'>
                        <GitHub sx={{ color: 'black' }} />
                    </IconButton>
                    <IconButton sx={{ marginX: 2 }} href='https://www.linkedin.com/in/sauldariush/' target='_blank'>
                        <LinkedIn sx={{ color: 'black' }} />
                    </IconButton>
                    <IconButton sx={{ marginX: 2 }} href='https://join.skype.com/invite/qQ6HFhPixsPa' target='_blank'>
                        <Phone sx={{ color: 'black' }} />
                    </IconButton>
                </Box>
                <Button variant='contained' sx={{ backgroundColor: '#bdbdbd', color: 'black' }} href='https://drive.google.com/file/d/1DRz87l6u5LFnCCbWefGhmsgPxm5mGgAQ/view?usp=share_link' target='_blank'>
                Read my Résumé
                </Button>
            </Grid>
            <Grid 
                item 
                xs={12} 
                md={6} 
                sx={{ 
                    order: { xs: 1, md: 2 }, 
                    display: { xs: 'flex', justifyContent: 'center' } 
                }}
            >
                <Avatar src='https://res.cloudinary.com/dlk7w16gw/image/upload/v1681567897/ywm0cxzso4looajegmsp.jpg' sx={{ width: {xs:300, sm: 400}, height: {xs:300, sm: 400} }} />
            </Grid>
        </Grid>

    </Box>
  )
}

export default AboutMe