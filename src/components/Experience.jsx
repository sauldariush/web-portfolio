import { Business, BusinessCenter, Code, OpenInNew, School } from '@mui/icons-material';
import { Box, Grid, Typography, styled, Paper, Slide, Card, Button, CardActions, CardMedia, CardContent, IconButton, CardActionArea } from '@mui/material'
import React from 'react'

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,

  }));

const skills = [ 
    { 
        "skill": "Front End Development",
        "description": "HTML 5, CSS 3, Google Chrome, Chrome Developer Tools, Bootstrap, Wireframes and Mockups",
        "url": "https://share.zertify.zuitt.co/certificate/03baf7aa-7523-45a3-bde3-2967defb7da5/"
    },
    {
        "skill": "Back End Development",
        "description": "JavaScript, MongoDB, Node.js, REST Architecture, Express.js, Postman, PHP, mySQL",
        "url": "https://share.zertify.zuitt.co/certificate/b46333ea-8003-46cf-9bcf-ccd58e856981/"
    },

    {
        "skill": "Full Stack Development",
        "description": "MERN tech stack (MongoDB, Express.js, React.js, Node.js)",
        "url": "https://share.zertify.zuitt.co/certificate/05606ac2-dfcc-4aec-a669-0900190a8a1d/"
    },
] 

const eductaion = [ 
    { 
        "school": "Zuitt Coding Bootcamp",
        "course": "Full Stack Web Development",
        "schoolYear": "Feb 2023 - Mar 2023",
        "location": "Online Certification Course"
    },
    {
        "school": "Lyceum of the Philippines University - Cavite",
        "course": "Electrical Engineering",
        "schoolYear": "SY: 2009 - 2014",
        "location": "Gen. Trias, Cavite"
    },

    {
        "school": "Imus Institute",
        "course": "High School",
        "schoolYear": "SY: 2005 - 2009",
        "location": "Imus, Cavite"
    },
]
const experience = [ 
    { 
        "company": "Crosslink Electric & Construction Corp.",
        "position": "Project In-charge",
        "companyYear": "July 2019 - Nov 2022", 
    },
    {
        "company": "Optimum Equipment Management & Exchange Inc.",
        "position": "Design and Estimate Engineer",
        "companyYear": "Sept 2016 - May 2019",
    },

    {
        "company": "Taikisha Philippines Inc.",
        "position": "Site Engineer/Design and Estimate Engineer",
        "companyYear": "Feb 2015 - Sept 2016",
    },
] 

function Experience() {
  return (
    <Box sx={{ marginTop: 3}} >
       
        
        <Grid container rowSpacing={3} columnSpacing={{ xs: 1, sm: 2, md: 3 }} sx={{ marginTop: 1, justifyContent: 'center' }} >
            
                <Grid item s={12} md={4}  >
                    <Typography variant='h4'sx={{ display: 'flex', alignItems: 'center' }}> <Code fontSize='large' sx={{marginX: 1, verticalAlign: 'middle'}} />Skills </Typography>
                    {skills.map((item) =>(
                    <Card key={item.skill} sx={{ marginY: 2, width: {xs: 300, sm:450, md: '100%' }, height: '28%'}} >
                        <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            {item.skill}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            {item.description}
                        </Typography>
                        <CardActionArea sx={{marginTop: 1}} >
                            <Button sx={{ backgroundColor: '#fff', color: 'black' }} href={item.url} target='_blank'>Show Credentials <OpenInNew fontSize='small' sx={{marginX: 1, verticalAlign: 'middle'}} /> </Button>
                        </CardActionArea>
                        </CardContent>
                    </Card>
                    ))}
                </Grid>
            
            <Grid item s={12} md={4} >
                <Typography variant='h4' sx={{ display: 'flex', alignItems: 'center' }}> <School fontSize='large' sx={{marginX: 1, verticalAlign: 'middle'}}/>Education </Typography>
                {eductaion.map((item) => (
                    <Card key={item.school} sx={{ marginY: 2, width: {xs: 300, sm:450, md: '100%' }, height: '28%'}} >
                        <CardContent>
                        <Typography variant="body1" color="text.secondary">
                            {item.schoolYear}
                        </Typography>
                        <Typography gutterBottom variant="h5" component="div">
                            {item.course}
                        </Typography>
                        <Typography variant="subtitle1" color="text.secondary">
                            {item.school}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            {item.location}
                        </Typography>
                        </CardContent>
                    </Card>
                ))}            
            </Grid>
            <Grid item s={12} md={4} >
                <Typography variant='h4' sx={{ display: 'flex', alignItems: 'center' }}> <BusinessCenter fontSize='large' sx={{marginX: 1, verticalAlign: 'middle'}}/>Experience </Typography>
                {experience.map((item) => (
                    <Card key={item.company} sx={{ marginY: 2, width: {xs: 300, sm:450, md: '100%' }, height: '28%'}} >
                        <CardContent>
                            <Typography variant="body2" color="text.secondary">
                                {item.companyYear}
                            </Typography>
                            <Typography gutterBottom variant="h5" component="div">
                                {item.position}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                {item.company}
                            </Typography>
                        </CardContent>
                    </Card> 
                ))}               
            </Grid>
        </Grid>      
    </Box>
    )
}

export default Experience