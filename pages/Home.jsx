  import { 
    Box, 
    Button, 
    Typography
  } from '@mui/material';
  import { Link as RouterLink} from 'react-router-dom';

  import React from 'react'


  const AboutLinkBehavior = React.forwardRef((props, ref) => (
    <RouterLink ref={ref} to="/about" {...props} role={undefined} />
  ));

  function Home({theme}) {

    return (
          
            <Box sx={{ marginTop: 20 }} >
              <Typography variant='h2'sx={{fontSize: {xs: 50, sm: 70}}}> 👋 Hi! I am </Typography>
              <Typography variant='h1'sx={{fontSize: {xs: 60, sm: 100}}} >Saul Darius Hernandez</Typography> 
              <Typography variant='body1' >an Electrical Engineer turned into a Software Engineer who gained a strong foundation in problem-solving and technical skills during the days before shifting career. I have recently completed a web development intensive bootcamp, where I honed my skills in web development, including HTML, CSS, JavaScript, and various web development frameworks. I am committed to learning and staying up-to-date with new technologies and trends in the field. With my enthusiasm and willingness to learn, I am excited to tackle new challenges and collaborate with clients to bring their visions to life. Let's work together to create beautiful, user-friendly websites that make a positive impact on people's lives.</ Typography>
              <Button variant='contained' sx={{ marginTop: 3, marginBottom:5, backgroundColor: '#bdbdbd', color: 'black' }} component={AboutLinkBehavior}>
                  More About Me
              </Button>
            </Box>
          



    )
  }

  export default Home