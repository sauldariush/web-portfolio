import { Box, Grid, Typography, styled, Paper, Card, Button, CardActions, CardMedia, CardContent } from '@mui/material'
import React from 'react'
import { Link as RouterLink} from 'react-router-dom';

const ContactLinkBehavior = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/contact" {...props} role={undefined} />
));

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,

  }));

function Projects() {
  return (
    <Box sx={{ marginTop: 20}} >
        <Typography variant='h3' >My Projects </Typography>
        <Grid container rowSpacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }} sx={{ marginTop: 2 }} >
            <Grid item xs={12} md={6} >
                <Card >
                    <CardMedia
                    sx={{ height: 300 }}
                    image="https://res.cloudinary.com/dlk7w16gw/image/upload/v1681131543/nd1jid1ywcvvxk0ysbwq.png"
                    />
                    <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        Web Portfolio
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        This Web portfolio is made using HTML, CSS and Bootstrap.
                    </Typography>
                    </CardContent>
                    <CardActions>
                    <Button size="small" href='https://gitlab.com/batch-253-ft/s13-14/capstone-1/-/tree/master/' target='_blank' >See Code</Button>
                    <Button size="small" href='https://dariusgithub14.github.io/webportfolio/' target='_blank' >Live View</Button>
                    </CardActions>
                </Card>
            </Grid>
            <Grid item xs={12} md={6} >
                <Card >
                    <CardMedia
                    sx={{ height: 300 }}
                    image="https://res.cloudinary.com/dlk7w16gw/image/upload/v1681131543/zzx12n71ak6ff3zvwhiy.png"
                    
                    />
                    <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        Ecommerce Web App
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        A simple Ecommerce Web App using MongoDB, Express, React and Nodejs.
                    </Typography>
                    </CardContent>
                    <CardActions>
                    <Button size="small" href='https://gitlab.com/sauldariush/e-commerce-web-app' target='_blank' >See Code</Button>
                    <Button size="small" href='https://e-commerce-web-app-theta.vercel.app' target='_blank'  >Live View</Button>
                    </CardActions>
                </Card>            
            </Grid>
            <Grid item xs={12} md={6} >
                <Card >
                    <CardMedia
                    sx={{ height: 300 }}
                    image="https://res.cloudinary.com/dlk7w16gw/image/upload/v1681135547/xihgvgtuskixfyadlqwr.png"
                    
                    />
                    <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        Mobile Chat App
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        A Chat System using React Native and Firebase.
                    </Typography>
                    </CardContent>
                    <CardActions>
                    <Button size="small" href='https://gitlab.com/sauldariush/simple-chat-app' target='_blank' >See Code</Button>
                    <Button size="small" href='https://expo.dev/@sauldariush/chat-app' target='_blank' >Live View</Button>
                    </CardActions>
                </Card>                
            </Grid>
        </Grid> 
        <Box sx={{textAlign: 'center', marginY: 3}} >
            <Button variant='contained' sx={{ marginTop: 5, marginX: 5, backgroundColor: '#bdbdbd', color: 'black', paddingX: 10, paddingY: 3}} component={ContactLinkBehavior} size='large' >
                    Let's Connect
            </Button>  
        </Box>
   
    </Box>
  )
}

export default Projects