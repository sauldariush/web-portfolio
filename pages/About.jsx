import { Box, Button } from '@mui/material'
import React from 'react';
import { Link as RouterLink} from 'react-router-dom';

import AboutMe from '../src/components/AboutMe';
import Experience from '../src/components/Experience';

const ProjectsLinkBehavior = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/projects" {...props} role={undefined} />
));
const GalleryLinkBehavior = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/gallery" {...props} role={undefined} />
));

const professionalExperience = []

function About() {
  return (
    <Box sx={{marginTop: 15}} >
      <AboutMe />
      <Experience />
      <Box sx={{textAlign: 'center', marginY: 5}} >
        <Button variant='contained' sx={{ backgroundColor: '#bdbdbd', color: 'black', margin: 2, paddingX: 10, paddingY: 2 }} component={ProjectsLinkBehavior}>See my Projects</Button>
        <Button variant='contained' sx={{ backgroundColor: '#bdbdbd', color: 'black', margin: 2, paddingX: 10, paddingY: 2 }} component={GalleryLinkBehavior}>See my Gallery</Button>
      </Box>
    </Box>
  )
}

export default About