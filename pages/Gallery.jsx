import React from 'react'
import { Box, ButtonBase, Typography } from '@mui/material';
import { styled } from '@mui/material';

const images = [
    {
      url: 'https://res.cloudinary.com/dlk7w16gw/image/upload/v1679688002/wngt11n69rglnysstf5b.jpg',
      title: 'Double Rainbow',
      width: '33%',
    },
    {
      url: 'https://res.cloudinary.com/dlk7w16gw/image/upload/v1679684813/z9yryo2837ndgdvfqa7w.jpg',
      title: 'Tennis Court',
      width: '34%',
    },
    {
      url: 'https://res.cloudinary.com/dlk7w16gw/image/upload/v1681126336/eyni6mc3g8itwichdwvs.jpg',
      title: 'Reflections',
      width: '33%',
    },
    {
      url: 'https://res.cloudinary.com/dlk7w16gw/image/upload/v1681126335/tklp93fduc9klgmjb8u3.jpg',
      title: 'Manila Yatch Club',
      width: '33%',
    },
    {
      url: 'https://res.cloudinary.com/dlk7w16gw/image/upload/v1681126336/cwptpqja0rinrknux1go.jpg',
      title: 'City Lights',
      width: '34%',
    },
    {
      url: 'https://res.cloudinary.com/dlk7w16gw/image/upload/v1681126334/p6skxaxynlyckx293kke.jpg',
      title: 'Island',
      width: '33%',
    },
  ];

  const ImageButton = styled(ButtonBase)(({ theme }) => ({
    position: 'relative',
    height: 200,
    [theme.breakpoints.down('sm')]: {
      width: '100% !important', // Overrides inline-style
      height: 100,
    },
    '&:hover, &.Mui-focusVisible': {
      zIndex: 1,
      '& .MuiImageBackdrop-root': {
        opacity: 0.15,
      },
      '& .MuiImageMarked-root': {
        opacity: 0,
      },
      '& .MuiTypography-root': {
        border: '4px solid currentColor',
      },
    },
  }));
  
  const ImageSrc = styled('span')({
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: 'cover',
    backgroundPosition: 'center 40%',
  });
  
  const Image = styled('span')(({ theme }) => ({
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.common.white,
  }));
  
  const ImageBackdrop = styled('span')(({ theme }) => ({
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: theme.palette.common.black,
    opacity: 0.4,
    transition: theme.transitions.create('opacity'),
  }));
  
  const ImageMarked = styled('span')(({ theme }) => ({
    height: 3,
    width: 18,
    backgroundColor: theme.palette.common.white,
    position: 'absolute',
    bottom: -2,
    left: 'calc(50% - 9px)',
    transition: theme.transitions.create('opacity'),
  })); 

function Gallery() {
  return (
    <Box sx={{ marginTop: 15, textAlign: 'center'}} >
      <Typography sx={{ marginY: 2}} variant='h3' >Welcome to my Gallery</Typography>
      <Typography sx={{ marginY: 2}} variant='body2' >I just want to showcase some of the photos I am most proud of</Typography>
        <Box sx={{ display: 'flex', flexWrap: 'wrap', minWidth: 300, width: '100%', minHeight: 200 }}>

        {images.map((image) => (
          <ImageButton
            focusRipple
            key={image.title}
            style={{
              width: image.width,
            }}
          >
            <ImageSrc style={{ backgroundImage: `url(${image.url})` }} />
            <ImageBackdrop className="MuiImageBackdrop-root" />
            <Image>
              <Typography
                component="span"
                variant="subtitle1"
                color="inherit"
                sx={{
                  position: 'relative',
                  p: 4,
                  pt: 2,
                  pb: (theme) => `calc(${theme.spacing(1)} + 6px)`,
                }}
              >
                {image.title}
                <ImageMarked className="MuiImageMarked-root" />
              </Typography>
            </Image>
          </ImageButton>
        ))}
        </Box>
    </Box>
);
}

export default Gallery