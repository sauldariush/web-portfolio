import { Alert, AlertTitle, Box, Button, Card, FormControl, Grid, Snackbar, TextField, Typography } from '@mui/material'
import React, { useEffect, useRef, useState } from 'react';
import emailjs from '@emailjs/browser';
import  Swal from 'sweetalert2';

function Contact() {
  const [ name, setName ] = useState("");
  const [ email, setEmail ] = useState("");
  const [ contactNum, setContactNum ] = useState("");
  const [ subject, setSubject ] = useState("");
  const [ message, setMessage ] = useState("")
  const [ isActive, setIsActive ] =useState(false);
 

  const form = useRef();


  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm(`service_5es7cht`, `template_ltk490a`, form.current, `em545QJYXCg5fxuKO`)
      .then((result) => {
          console.log(result.text);
          console.log("message sent")

          setName("");
          setEmail("");
          setContactNum("");
          setSubject("");
          setMessage("");

          Swal.fire({
            icon: 'success',
            title: 'Message Sent!',
            showConfirmButton: false,
            timer: 1500
          });

      }, (error) => {
          console.log(error.text);
      });
 
  }

  useEffect(() => {
    if( name !== '' && email !== '' && contactNum !== '' && subject !== '' && message !== ''){
      setIsActive(true);
    } else{
      setIsActive(false);
    }
  }, [name, email, contactNum, subject, message])

  return (

    <Grid container justifyContent="center" alignItems="center">
      <Box sx={{ marginTop: 15}}>
        
        <Typography variant='h4'sx={{textAlign: 'center'}} >
            Keep in touch!
        </Typography>
        <form ref={form} onSubmit={sendEmail} >
          <FormControl sx={{minWidth: {xs: 250, sm: 400, md: 600}}} >
          
            <TextField 
              required 
              sx={{marginY: 1}} 
              id="standard-basic" 
              label="Full Name" 
              variant="standard" 
              type='name'
              name="user_name" 
              onChange={e => setName(e.target.value)} 
              value={name} 
            />
            <TextField 
              sx={{marginY: 1}} 
              id="standard-basic" 
              label="Email" 
              variant="standard" 
              name="user_email" 
              type='email'
              onChange={e => setEmail(e.target.value)} 
              value={email} 
              required
            />
            <TextField 
              required sx={{marginY: 1}} 
              id="standard-basic" 
              label="Contact Number" 
              variant="standard" 
              name="user_contactNum" 
              onChange={e => setContactNum(e.target.value)} 
              value={contactNum} 
            />
            <TextField 
              required 
              sx={{marginY: 1}} 
              id="standard-basic" 
              label="Subject" 
              variant="standard" 
              name="contact_subject" 
              onChange={e => setSubject(e.target.value)} 
              value={subject} 
            />
            <TextField
                required
                sx={{marginY: 2}}
                id="standard-multiline-flexible"
                label="Message"
                multiline
                maxRows={6}
                variant="standard"
                name="message"
                onChange={e => setMessage(e.target.value)}
                value={message}   
              />
              <Button variant="contained" type="submit" disabled={!isActive}>Submit</Button>
          </FormControl>
        </form>
      </Box>
    </Grid>
  )
}

export default Contact